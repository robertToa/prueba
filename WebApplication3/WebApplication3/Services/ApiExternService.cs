﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApplication3.Services
{

    public class ApiExternService
    {
        private static HttpClient HttpClient;
        private static readonly String URL = "https://rickandmortyapi.com/";

        public ApiExternService()
        {
            HttpClient = new HttpClient();
            HttpClient.BaseAddress = new Uri(URL);
        }

        public dynamic serviceExtern(){
            var request = HttpClient.GetAsync("api/character").Result;
            if (request.IsSuccessStatusCode)
                return request.Content.ReadAsStringAsync().Result;
            else
                throw new Exception("API no encontrada");
        }

        public dynamic serviceExternGetObj(string id)
        {
            var request = HttpClient.GetAsync(id).Result;
            if (request.IsSuccessStatusCode)
                return request.Content.ReadAsStringAsync().Result;
            else
                throw new Exception("API no encontrada");
        }
    }
}
