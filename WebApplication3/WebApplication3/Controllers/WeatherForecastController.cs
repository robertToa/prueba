﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Entities;
using WebApplication3.Model;
using WebApplication3.Services;

namespace WebApplication3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly ApiExternService api;
        private readonly AplicationDbCOntext _context;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
            api = new ApiExternService();
        }

        /*public WeatherForecastController(AplicationDbCOntext context)
        {
            _context = context;
            api = new ApiExternService();
        }*/


        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var data = api.serviceExtern();
                return Ok(data);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPersonajeById([FromRoute] string id)
        {
            try
            {
                var data = api.serviceExternGetObj(id);
                return Ok(data);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> PostPersonaje([FromBody] Personaje personaje)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var persona = await _context.Personaje.FindAsync(personaje.Id);
            if (persona == null)
            {
                _context.Personaje.Add(personaje);
                await _context.SaveChangesAsync();
                return CreatedAtAction("Personaje", new { id = personaje.Id }, personaje);
            }else
                return BadRequest("El personaje ya existe");
        }
    }
}
