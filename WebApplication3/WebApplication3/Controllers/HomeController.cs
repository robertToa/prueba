﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Services;

namespace WebApplication3.Entities
{
    public class HomeController : Controller
    {
        private readonly ApiExternService api;

        [HttpGet("/api")]
        public async Task<IActionResult> GetApiExtern()
        {
            try
            {
                return api.serviceExtern();
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
