﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Entities;

namespace WebApplication3.Model
{
    public partial class AplicationDbCOntext: DbContext
    {
        public AplicationDbCOntext(DbContextOptions<AplicationDbCOntext> options
            ): base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            DBPresonaje(modelBuilder);
        }


        public virtual DbSet<Personaje> Personaje { get; set; } 
        private void DBPresonaje(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Personaje>(entit =>
            {
                entit.ToTable("Personaje");
                entit.HasKey(e => e.Id);
                entit.Property(e => e.Id).HasColumnName("Id");
                entit.Property(e => e.Nombre).HasColumnName("Nombre");
                entit.Property(e => e.Fecha).HasColumnName("Fecha");
                entit.Property(e => e.InformacionPersonaje).HasColumnName("Info_Personaje");

            });
        }
    }
}
