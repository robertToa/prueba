import { DataDto } from "./data.dto";

export interface ResultApiDto {
    info: any,
    results: DataDto[]
}
