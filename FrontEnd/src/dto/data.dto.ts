export interface DataDto {
    id: number,
    name: string,
    image:string,
    enable: boolean
}
