import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataDto } from 'src/dto/data.dto';
import { ResultApiDto } from 'src/dto/result-api.dto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExternalApiService {

  private nombreModelo = '/weatherforecast';
  constructor(
    private readonly httpClientService: HttpClient,
  ) { }

  getDataApi(): Observable<ResultApiDto> {
    return this.httpClientService
      .get<ResultApiDto>(environment.urlServer + this.nombreModelo);
  }

  getDataApiById(id: number): Observable<DataDto> {
    return this.httpClientService
      .get<DataDto>(environment.urlServer + this.nombreModelo + "/" + id);
  }

  createPersonaje(objeto: DataDto): Observable<DataDto> {
    const url = environment.urlServer + this.nombreModelo;
    return this.httpClientService
      .post<DataDto>(url, objeto);
  }

}
