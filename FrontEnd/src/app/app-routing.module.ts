import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaTarjetasComponent } from './pages/lista-tarjetas/lista-tarjetas.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'tarjetas',
    pathMatch: 'full',
  },
  {
    path: 'tarjetas',
    component: ListaTarjetasComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
