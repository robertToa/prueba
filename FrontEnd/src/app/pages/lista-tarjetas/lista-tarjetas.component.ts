import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DataDto } from 'src/dto/data.dto';
import { ExternalApiService } from 'src/service/external-api.service';

@Component({
  selector: 'app-lista-tarjetas',
  templateUrl: './lista-tarjetas.component.html',
  styleUrls: ['./lista-tarjetas.component.css']
})
export class ListaTarjetasComponent implements OnInit {

  listaTarjetas: DataDto[] = [];
  formularioFormGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private readonly apiExternalService: ExternalApiService,
  ) {
    this.formularioFormGroup = this.fb.group({
      descripcion: new FormControl('', Validators.required),
    });
   }

  ngOnInit(): void {
    const apiExt$ = this.apiExternalService.getDataApi();
    apiExt$.subscribe(
      (obj) => {
        if(obj.results && obj.results.length>0){
          this.listaTarjetas = obj.results;
          this.listaTarjetas.forEach(e => e.enable=false);
      
        }
      },
      error1 => console.log("asdsadsa")
    );
  }

  onClickGuardar(valuesForm: any, tarjeta: DataDto) {
    if (valuesForm.valid) {
      this.apiExternalService.createPersonaje(tarjeta).subscribe( obj => {
        var indice = this.listaTarjetas.findIndex(e => e.id==tarjeta.id);
        this.listaTarjetas.splice(indice, 1);
      });
    } else {
      Object.keys(valuesForm.controls).forEach(key => {
        valuesForm.controls[key].markAsDirty();
      });
    }
  }

}
